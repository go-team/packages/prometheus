From: Martina Ferrari <tina@debian.org>
Date: Sat, 20 Jun 2020 16:03:13 -0300
Subject: Disable TSDB lockfile

Forwarded: not-needed
Last-Update: 2022-10-25

Stop creating a tsdb lockfile by default. Replace
storage.tsdb.no-lockfile flag with storage.tsdb.use-lockfile, so the
default is not to create a lockfile, while still allowing people to
request the feature.
---
 cmd/prometheus/main.go | 18 +++++++++---------
 1 file changed, 9 insertions(+), 9 deletions(-)

diff --git a/cmd/prometheus/main.go b/cmd/prometheus/main.go
index 8db2f2c..e4efc50 100644
--- a/cmd/prometheus/main.go
+++ b/cmd/prometheus/main.go
@@ -364,8 +364,8 @@ func main() {
 	serverOnlyFlag(a, "storage.tsdb.retention.size", "Maximum number of bytes that can be stored for blocks. A unit is required, supported units: B, KB, MB, GB, TB, PB, EB. Ex: \"512MB\". Based on powers-of-2, so 1KB is 1024B.").
 		BytesVar(&cfg.tsdb.MaxBytes)
 
-	serverOnlyFlag(a, "storage.tsdb.no-lockfile", "Do not create lockfile in data directory.").
-		Default("false").BoolVar(&cfg.tsdb.NoLockfile)
+	serverOnlyFlag(a, "storage.tsdb.use-lockfile", "Create a lockfile in data directory.").
+		Default("false").BoolVar(&cfg.tsdb.UseLockfile)
 
 	// TODO: Remove in Prometheus 3.0.
 	var b bool
@@ -409,8 +409,8 @@ func main() {
 		"Maximum age samples may be before being forcibly deleted when the WAL is truncated").
 		SetValue(&cfg.agent.MaxWALTime)
 
-	agentOnlyFlag(a, "storage.agent.no-lockfile", "Do not create lockfile in data directory.").
-		Default("false").BoolVar(&cfg.agent.NoLockfile)
+	agentOnlyFlag(a, "storage.agent.use-lockfile", "Create a lockfile in data directory.").
+		Default("false").BoolVar(&cfg.agent.UseLockfile)
 
 	a.Flag("storage.remote.flush-deadline", "How long to wait flushing sample on shutdown or config reload.").
 		Default("1m").PlaceHolder("<duration>").SetValue(&cfg.RemoteFlushDeadline)
@@ -1174,7 +1174,7 @@ func main() {
 					"MinBlockDuration", cfg.tsdb.MinBlockDuration,
 					"MaxBlockDuration", cfg.tsdb.MaxBlockDuration,
 					"MaxBytes", cfg.tsdb.MaxBytes,
-					"NoLockfile", cfg.tsdb.NoLockfile,
+					"UseLockfile", cfg.tsdb.UseLockfile,
 					"RetentionDuration", cfg.tsdb.RetentionDuration,
 					"WALSegmentSize", cfg.tsdb.WALSegmentSize,
 					"WALCompression", cfg.tsdb.WALCompression,
@@ -1689,7 +1689,7 @@ type tsdbOptions struct {
 	MaxBlockChunkSegmentSize       units.Base2Bytes
 	RetentionDuration              model.Duration
 	MaxBytes                       units.Base2Bytes
-	NoLockfile                     bool
+	UseLockfile                    bool
 	WALCompression                 bool
 	WALCompressionType             string
 	HeadChunksWriteQueueSize       int
@@ -1710,7 +1710,7 @@ func (opts tsdbOptions) ToTSDBOptions() tsdb.Options {
 		MaxBlockChunkSegmentSize:       int64(opts.MaxBlockChunkSegmentSize),
 		RetentionDuration:              int64(time.Duration(opts.RetentionDuration) / time.Millisecond),
 		MaxBytes:                       int64(opts.MaxBytes),
-		NoLockfile:                     opts.NoLockfile,
+		NoLockfile:                     !opts.UseLockfile,
 		WALCompression:                 wlog.ParseCompressionType(opts.WALCompression, opts.WALCompressionType),
 		HeadChunksWriteQueueSize:       opts.HeadChunksWriteQueueSize,
 		SamplesPerChunk:                opts.SamplesPerChunk,
@@ -1735,7 +1735,7 @@ type agentOptions struct {
 	StripeSize             int
 	TruncateFrequency      model.Duration
 	MinWALTime, MaxWALTime model.Duration
-	NoLockfile             bool
+	UseLockfile            bool
 }
 
 func (opts agentOptions) ToAgentOptions() agent.Options {
@@ -1746,7 +1746,7 @@ func (opts agentOptions) ToAgentOptions() agent.Options {
 		TruncateFrequency: time.Duration(opts.TruncateFrequency),
 		MinWALTime:        durationToInt64Millis(time.Duration(opts.MinWALTime)),
 		MaxWALTime:        durationToInt64Millis(time.Duration(opts.MaxWALTime)),
-		NoLockfile:        opts.NoLockfile,
+		NoLockfile:        !opts.UseLockfile,
 	}
 }
 
